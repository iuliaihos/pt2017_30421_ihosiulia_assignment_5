package view;

import javax.swing.JButton;
import javax.swing.JFrame;
import net.miginfocom.swing.MigLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import data.DataManipulation;
import data.MonitoredData;

import javax.swing.BoxLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Frame
{

	private JFrame frame;
	private JButton daysCount;
	private JButton activityCount;
	private JButton mapDaysToActivityCount;
	private JButton filterActivitiesByTotalTime;
	private JButton filterActivitiesByInterval;
	private JLabel lblActivity;
	private DataManipulation dataManipulation;
	
	public Frame(DataManipulation d)
	{
    dataManipulation = d;
	frame = new JFrame("Homework 5");
	frame.setSize(900,700);
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.getContentPane().setLayout(null);
	
	lblActivity = new JLabel("<html>2 -> activity count.txt<br>3 -> activity and total interval - filtered.txt<br>4 -> days with activity count.txt"
			+ "<br>5 -> activities filtered.txt </html>");
	lblActivity.setFont(new Font("Yu Gothic", Font.BOLD, 20));
	
	lblActivity.setBounds(15, 193, 230, 262);
	lblActivity.setVisible(true);
	frame.getContentPane().add(lblActivity);
	daysCount = new JButton("<html> Count the days<br>from the monitored data</html");
	
	daysCount.addActionListener((e)-> JOptionPane.showMessageDialog(null,"The day count is: " + dataManipulation.countDistinctDays()  ));
	daysCount.setBounds(277, 80, 255, 69);
	frame.getContentPane().add(daysCount);
	activityCount = new JButton("<html>Count the occurrences<br>for each activity</html>");
	
	activityCount.addActionListener(
			(e)->{dataManipulation.mapActivityCount();
			JOptionPane.showMessageDialog(null,"The result can be found in <activity count.txt>");	
	});
	activityCount.setBounds(277, 165, 255, 65);
	frame.getContentPane().add(activityCount);
	filterActivitiesByTotalTime = new JButton("<html>Output only the activities<br> with total time<br> less than 10 hours</html>");
	
	filterActivitiesByTotalTime.addActionListener(
			(e)->{dataManipulation.mapActivityToDuration();
	             JOptionPane.showMessageDialog(null,"The result can be found in <activity and total interval - filtered.txt>");});
	filterActivitiesByTotalTime.setBounds(277, 246, 255, 88);
	frame.getContentPane().add(filterActivitiesByTotalTime);
	mapDaysToActivityCount = new JButton("<html>For each day, output all<br> activities, together with their<br> count</html>");
	
	mapDaysToActivityCount.addActionListener(
			(e)->{dataManipulation.mapDayToActivityCount();
            JOptionPane.showMessageDialog(null,"The result can be found in <days with activity count.txt>");});
	mapDaysToActivityCount.setBounds(277, 350, 252, 93);
	frame.getContentPane().add(mapDaysToActivityCount);
	filterActivitiesByInterval = new JButton("<html>Filter the activities that have <br>more than 90% of the<br> intervals less than 5 minutes</html>");
	
	filterActivitiesByInterval.addActionListener(
			(e)-> {dataManipulation.filterActivities();
			       JOptionPane.showMessageDialog(null, "The result can be found in <activities filtered.txt>");});
	filterActivitiesByInterval.setBounds(277, 459, 255, 93);
	frame.getContentPane().add(filterActivitiesByInterval);
	
	frame.setVisible(true);
	}
}

package data;

import java.time.LocalDateTime;

public class MonitoredData {
	private LocalDateTime startTime;
	private LocalDateTime endTime;
	private String activityLabel;
	private LocalDateTime timeInterval;
	
	public MonitoredData(String data)
	{
	String[] split = data.split("[\\s:-]+"); //splits by spaces, '-' and  ':' 
	startTime = LocalDateTime.of(Integer.parseInt(split[0]), Integer.parseInt(split[1]), 
			                     Integer.parseInt(split[2]) , Integer.parseInt(split[3]),
			                     Integer.parseInt(split[4]), Integer.parseInt(split[5]));
	endTime = LocalDateTime.of(Integer.parseInt(split[6]), Integer.parseInt(split[7]), 
							   Integer.parseInt(split[8]) , Integer.parseInt(split[9]),
							   Integer.parseInt(split[10]), Integer.parseInt(split[11]));
	activityLabel = split[12];
	setTimeInterval();
	}
	
	public LocalDateTime getStartTime() {
		return startTime;
	}
	
	public int getStartDay() {
		return startTime.getDayOfYear();
	}
	
	public int getEndDay() {
		return endTime.getDayOfYear();
	}
	
	public LocalDateTime getTimeInterval() {
		return timeInterval;
	}
	
	public void setTimeInterval()
	{
		int h = endTime.getHour()-startTime.getHour(); 
		int o = endTime.getMinute()-startTime.getMinute();
		int s = endTime.getSecond()-startTime.getSecond();
		if(h< 0)
	    	h = 24 - startTime.getHour() + endTime.getHour();
		if(o < 0)
		    {
		    	h -= 1;
		    	o = 60 - startTime.getMinute() + endTime.getMinute();
		    }
		if (s<0) 
		    {
			     o -= 1;
			     s = 60 - startTime.getSecond() + endTime.getSecond(); 
		    }
	  
		timeInterval =  LocalDateTime.of(Math.abs(endTime.getYear()-startTime.getYear()),1,1,h,o,s);
	}
	
	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}
	
	public LocalDateTime getEndTime() {
		return endTime;
	}
	
	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}
	
	public String getActivityLabel() {
		return activityLabel;
	}
	
	public void setActivityLabel(String activityLabel) {
		this.activityLabel = activityLabel;
	}
	
	public String toString()
	{
		String s = "";
		s += startTime.getYear()+ "-" +startTime.getMonthValue()+ "-" +startTime.getDayOfMonth() + " " +
		     +startTime.getHour() + ":" + startTime.getMinute() + ":" + startTime.getSecond();
		s += "\t" +endTime.getYear()+ "-" + endTime.getMonthValue()+ "-" + endTime.getDayOfMonth() + " " +
			     +endTime.getHour() + ":" + endTime.getMinute() + ":" + endTime.getSecond();
		s += "\t" + activityLabel;
		return s;
		
	   
	}

}

package data;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import io.FileInputOutput;

public class DataManipulation {
	private List<MonitoredData> data;
	private Map<String,Integer> activityCount;
	private Map<String, LocalDateTime> activityToTotalInterval; 
	private List<String> activitiesFiltered;
	private Map<String,List<LocalDateTime>> activitiesToInterval;
	private Map<Integer, Map<String,Integer>> mapDayToMapActivityCount;
	
	public DataManipulation()
	{
		data = new ArrayList<MonitoredData>();
		setData(FileInputOutput.readDataFromFile());
		activityCount = new HashMap<String, Integer>();
		activityToTotalInterval = new HashMap<String, LocalDateTime>();
		mapDayToMapActivityCount = new HashMap<Integer, Map<String,Integer>>();
		activitiesFiltered = new ArrayList<String>();
		activitiesToInterval= new HashMap<String, List<LocalDateTime>>();
		activitiesToInterval = mapActivitiesToInterval();
	}
	
	
	public int countDistinctDays()

	{
		int count = (int)data.stream()
		                     .map(a -> a.getStartTime().getDayOfYear())
				             .distinct()
				             .count();
		MonitoredData last = data.get(data.size()-1);
		if (last.getStartDay() != last.getEndDay()) count += 1;
		return count;
	}
	
		
	public void mapActivityCount()
	{
		activityCount = data.stream()
				  .collect(Collectors.groupingBy(MonitoredData::getActivityLabel,Collectors.summingInt(a->1)));
		List<String> lines = new ArrayList<String>();
		for (Map.Entry<String,Integer> m : activityCount.entrySet())
			lines.add(m.getKey() + " - " + m.getValue());
		FileInputOutput.writeToTextFile("activity count.txt",lines);   
	}
	

	public Map<String,Integer> mapActivityToFrequency(List<String> list)
	{
		Map<String,Integer> result  =new HashMap<String, Integer>();  
		result = list.stream()
		             .collect(Collectors.groupingBy(p->p,Collectors.summingInt(p->1)));
		return result;
	}
	
	public void mapDayToActivityCount()
	{
	Map<Integer,List<String>> t = data.stream()
			   .collect(Collectors.groupingBy(MonitoredData::getStartDay,
					   						Collectors.mapping (MonitoredData::getActivityLabel, Collectors.toList())));
    
    t.entrySet().stream().forEach(
    			  entry->
    			  {
                  mapDayToMapActivityCount.put(entry.getKey()-data.get(0).getStartDay()+1,mapActivityToFrequency(entry.getValue()) );
    		      }
    		      );
    		
     List<String> sorted = mapDayToMapActivityCount.entrySet().stream()
    		                                                  .sorted( (o1, o2) -> o1.getKey().compareTo(o2.getKey()) )
    		                                                  .map(p -> p.toString())
                                                              .collect(Collectors.toList());
  
     //sorted.stream().forEach(System.out::println);
     FileInputOutput.writeToTextFile("days with activity count.txt",sorted);
	}
	
	private Map<String,List<LocalDateTime>> mapActivitiesToInterval()
	{
		return data.stream().collect(Collectors.groupingBy(MonitoredData::getActivityLabel,
				Collectors.mapping(MonitoredData::getTimeInterval,Collectors.toList())));
	
	}	
	public void mapActivityToDuration()
	{
		activitiesToInterval.entrySet().stream().forEach(p->
			{   int hour = 0, minute = 0, second = 0, day = 0;
				for (LocalDateTime i: p.getValue())
					{hour += i.getHour();
					minute += i.getMinute();
					second += i.getSecond();}
				if (second>=60)
				{minute += second/60;
				 second = second % 60;}
				if (minute>=60)
				{hour += minute/60;
				 minute = minute%60;}
				if(hour>=24)
				{ day = hour/24;
				  hour = hour%24;}
				LocalDateTime time = LocalDateTime.of(1,1,day+1,hour,minute,second);
				activityToTotalInterval.put(p.getKey(),time);	
			});
	activityToTotalInterval = activityToTotalInterval.entrySet().stream().filter(p-> p.getValue().getDayOfMonth()==1 && p.getValue().getHour()<=10)
						  .collect(Collectors.toMap(Entry::getKey,Entry::getValue));
	List <String> lines = new ArrayList<String>	();
	activityToTotalInterval.entrySet().stream().forEach(e -> 
										{
										lines.add(e.getKey() + " - " + e.getValue().getHour() + " hour(s) " + e.getValue().getMinute() + " minute(s) " + e.getValue().getSecond() + " second(s)");
										});
	FileInputOutput.writeToTextFile("activity and total interval - filtered.txt",lines);
	}

	
	private boolean lessThan5(Map.Entry<String,List<LocalDateTime>> entry) 
	{
		entry.getValue().stream().forEach(System.out::println);
		int initial = entry.getValue().size();
		List<LocalDateTime> shrinked  = entry.getValue().stream().filter( p-> p.getHour()==0 && p.getMinute()<4 ).collect(Collectors.mapping(p -> p, Collectors.toList()));
		System.out.println(shrinked.size() + "  "+ initial);
		if (shrinked.size() >= 0.9*initial) return true;
		return false;
	}
	
	
	public void filterActivities()
	{
		activitiesFiltered = activitiesToInterval.entrySet().stream().filter((p)-> {System.out.println(p.getKey());return !lessThan5(p);}).map(p -> p.getKey()).collect(Collectors.mapping(p->p, Collectors.toList()));
		FileInputOutput. writeToTextFile("activities filtered.txt",activitiesFiltered);
	}
	
	public String toString()
	{
		String s = "";
		for (MonitoredData m : data)
			s+="\n"+m.toString();
		return s;
	}
	public List<MonitoredData> getData() {
		return data;
	}
	public void setData(List<MonitoredData> data) {
		this.data = data;
	}
}

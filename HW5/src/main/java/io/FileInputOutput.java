package io;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import data.MonitoredData;

public class FileInputOutput {

	public static List<MonitoredData> readDataFromFile()
	{
		List<MonitoredData> md = new ArrayList<MonitoredData>();
		String filename = "D:\\PT2017_30421_IhosIulia_Assignment_5\\HW5\\Activity.txt";
		try (Stream<String> stream = Files.lines(Paths.get(filename)))
		{
	
			md =  stream.map(s-> new MonitoredData(s))
					    .collect(Collectors.toList());
                         
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	
		return md;	
	}
	
	public static void writeToTextFile(String filename, List<String> content)
	{
		Path file = Paths.get(filename);
		try {
			Files.write(file, content);
		} catch (IOException e) {
			e.printStackTrace();
		}
		   
	}
}
